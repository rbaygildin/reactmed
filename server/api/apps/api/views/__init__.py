from .med_area_views import *
from .med_test_views import *
from .real_ind_views import *
from .int_ind_views import *
from .text_ind_views import *
from .user_views import *
from .singup_views import *
from .test_rec_views import *
